import java.util.Scanner;

public class TahunKabisat{
    public static void main(String[] agrs){
        Scanner reader=new Scanner(System.in);
        int angka;
        System.out.println("Jika Selesai, isi dengan angka 0");
        do{
            angka=reader.nextInt();
            if(angka==0){
                System.out.println("TerimaKasih");
            }
            else if(angka%400==0){
                System.out.println("Kabisat");
            }
            else {
                if(angka%100==0){
                    System.out.println("Bukan Kabisat");
                }
                else {
                    if(angka%4==0){
                        System.out.println("Kabisat");
                    }
                    else{
                        System.out.println("Bukan Kabisat");
                    }
                }
            }
        }while(angka!=0);
    }
}