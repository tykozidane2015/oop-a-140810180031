import java.util.Scanner;

public class Kalimat{
    public static void main(String[] agrs){
        Scanner reader=new Scanner(System.in);
        String kal;
        int p, count=0;
        System.out.println("Input Kalimat    : ");
        kal = reader.nextLine();
        p=kal.length();
        System.out.println("Input Huruf : ");
        char a= reader.next().charAt(0);
        char[] arr= kal.toCharArray();
        for (int i=0; i<kal.length();i++){
            if(arr[i]==a){
                count++;
            }
            else if(arr[i]==' ' || arr[i]==','||arr[i]=='.'||arr[i]=='!'||arr[i]=='?'){
                p--;
            }
        }
        System.out.println("Banyak Huruf    : " + p);
        System.out.println("Banyak Huruf '"+a+"'    : "+count);
    }
}